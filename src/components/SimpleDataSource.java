/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package components;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author Ralph
 */
public class SimpleDataSource {
    
    private static Connection db_connection;
    private static String db_host;
    private static int db_port;
    private static String db_user;
    private static String db_pass;
    private static String db_db;
    
    public static Connection getInstance() throws SQLException
    {
        if(db_connection == null)
        {
            initDb();
            String connectionString = "jdbc:mysql://" + db_host + ":" + db_port +
                    "/" + db_db + "?user=" + db_user + "&password=" + db_pass;
            
            db_connection = DriverManager.getConnection(connectionString);
        }
        
        return db_connection;
    }
    
    private static void initDb()
    {
        db_host = "localhost";
        db_port = 3306;
        db_user = "root";
        db_pass = "root";
        db_db = "sportivodb";
    }
}
