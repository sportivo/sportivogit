/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.util.ArrayList;
import javax.swing.DefaultComboBoxModel;

/**
 *
 * @author dunyamin
 */
public class DocentNieuw extends javax.swing.JFrame {

    private DocentMain DocentMain;
    private model.PersoonModel PersoonModel;
    /**
     * Creates new form DocentNieuw
     */
    public DocentNieuw() {
        initComponents();
        this.vulSportBox();
    }
    public void vulSportBox()
    {
        ArrayList<model.PersoonModel> alDocenten = model.PersoonModel.findAllDocenten();
        
        DefaultComboBoxModel dcbm = new DefaultComboBoxModel(alDocenten.toArray());
        
        sportBox.setModel(dcbm);
    }
    
     public DocentMain getDocentMain() {
        return DocentMain;
    }

    public void setDocentMain(DocentMain frame){
        this.DocentMain = frame;
    }

    public model.PersoonModel getPersoonModel() {
        return this.PersoonModel;
    }

    public void setPersoonModel(model.PersoonModel PersoonModel) {
        this.PersoonModel = PersoonModel;
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        naam = new javax.swing.JTextField();
        adres = new javax.swing.JTextField();
        postcode = new javax.swing.JTextField();
        woonplaats = new javax.swing.JTextField();
        telefoonnummer = new javax.swing.JTextField();
        emailadres = new javax.swing.JTextField();
        bankrekening = new javax.swing.JTextField();
        btnOpslaan = new javax.swing.JButton();
        jLabel8 = new javax.swing.JLabel();
        sportBox = new javax.swing.JComboBox();
        jLabel9 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jLabel1.setText("Naam");

        jLabel2.setText("Adres");

        jLabel3.setText("Postcode");

        jLabel4.setText("Woonplaats");

        jLabel5.setText("Telefoonnummer");

        jLabel6.setText("E-mailadres");

        jLabel7.setText("Bankrekening");

        btnOpslaan.setText("Opslaan");
        btnOpslaan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOpslaanActionPerformed(evt);
            }
        });

        jLabel8.setText("Sport");

        sportBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        sportBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sportBoxActionPerformed(evt);
            }
        });

        jLabel9.setText("Maak nieuwe docent aan");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(63, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel7)
                                    .addComponent(jLabel8))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel2)
                                        .addComponent(jLabel3)
                                        .addComponent(jLabel4)
                                        .addComponent(jLabel9))
                                    .addGap(44, 44, 44))
                                .addGroup(layout.createSequentialGroup()
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel1)
                                        .addComponent(jLabel5)
                                        .addComponent(jLabel6))
                                    .addGap(83, 83, 83))))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(postcode, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(woonplaats, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(adres)
                            .addComponent(sportBox, 0, 59, Short.MAX_VALUE)
                            .addComponent(bankrekening)
                            .addComponent(naam, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(telefoonnummer, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(emailadres, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addGap(153, 153, 153))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(btnOpslaan)
                        .addContainerGap())))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel9)
                .addGap(23, 23, 23)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(naam, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(adres, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(postcode, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addGap(8, 8, 8)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(woonplaats, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addGap(5, 5, 5)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(telefoonnummer, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(emailadres, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(bankrekening, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(sportBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 15, Short.MAX_VALUE)
                .addComponent(btnOpslaan)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    
public void vulScherm(model.PersoonModel PersoonModel){
        this.setPersoonModel(PersoonModel);
        naam.setText(PersoonModel.getNaam());
        adres.setText(PersoonModel.getAdres());
        postcode.setText(PersoonModel.getPostcode());
        woonplaats.setText(PersoonModel.getWoonplaats());
        telefoonnummer.setText(PersoonModel.getTelefoon());
        emailadres.setText(PersoonModel.getEmailAdres());
        bankrekening.setText(PersoonModel.getEmailAdres());
        //sportBox.setText(PersoonModel.getPostcode());
    }
        
        
    private void btnOpslaanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnOpslaanActionPerformed
        // TODO add your handling code here:
        model.PersoonModel PersoonModel;
        if(this.getPersoonModel() == null)
            PersoonModel = new model.PersoonModel();
        else 
            PersoonModel = this.getPersoonModel();
        
        PersoonModel.setNaam(naam.getText());
        PersoonModel.setAdres(adres.getText());
        PersoonModel.setPostcode(postcode.getText());
        PersoonModel.setWoonplaats(woonplaats.getText());
        PersoonModel.setTelefoon(telefoonnummer.getText());
        PersoonModel.setEmailAdres(emailadres.getText());
        PersoonModel.setBankrekening(bankrekening.getText());
        PersoonModel.saveDocent();    
        
        DocentMain.vulDocentLijst();
        this.getDocentMain().enableButtons(true);
        this.dispose();
    }//GEN-LAST:event_btnOpslaanActionPerformed

    private void sportBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sportBoxActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_sportBoxActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /*
         * Set the Nimbus look and feel
         */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /*
         * If Nimbus (introduced in Java SE 6) is not available, stay with the
         * default look and feel. For details see
         * http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(DocentNieuw.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(DocentNieuw.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(DocentNieuw.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(DocentNieuw.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /*
         * Create and display the form
         */
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                new DocentNieuw().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField adres;
    private javax.swing.JTextField bankrekening;
    private javax.swing.JButton btnOpslaan;
    private javax.swing.JTextField emailadres;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JTextField naam;
    private javax.swing.JTextField postcode;
    private javax.swing.JComboBox sportBox;
    private javax.swing.JTextField telefoonnummer;
    private javax.swing.JTextField woonplaats;
    // End of variables declaration//GEN-END:variables
}
