/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.util.ArrayList;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;

/**
 *
 * @author dunyamin
 */
public class DocentMain extends javax.swing.JFrame {

    /**
     * Creates new form DocentMain
     */
    public DocentMain() {
        initComponents();
        
        this.vulDocentLijst();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        docentList = new javax.swing.JList();
        btnNieuweDocent = new javax.swing.JButton();
        btnAanpassenDocent = new javax.swing.JButton();
        btnDeleteDocent = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jLabel1.setText("Docenten");

        docentList.setModel(new javax.swing.AbstractListModel() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        docentList.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                docentListValueChanged(evt);
            }
        });
        jScrollPane1.setViewportView(docentList);

        btnNieuweDocent.setText("Nieuwe Docent");
        btnNieuweDocent.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNieuweDocentActionPerformed(evt);
            }
        });

        btnAanpassenDocent.setText("Aanpassen");
        btnAanpassenDocent.setEnabled(false);
        btnAanpassenDocent.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAanpassenDocentActionPerformed(evt);
            }
        });

        btnDeleteDocent.setText("Verwijderen");
        btnDeleteDocent.setEnabled(false);
        btnDeleteDocent.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteDocentActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 189, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnNieuweDocent)
                            .addComponent(btnAanpassenDocent)
                            .addComponent(btnDeleteDocent))))
                .addContainerGap(78, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnNieuweDocent)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnAanpassenDocent)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnDeleteDocent))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 163, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(111, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    
        public void vulDocentLijst()
    {
        ArrayList<model.PersoonModel> alDocenten = model.PersoonModel.findAllDocenten();
        
        DefaultComboBoxModel dcbm = new DefaultComboBoxModel(alDocenten.toArray());
        
        docentList.setModel(dcbm);
    }
        
    public void enableButtons(boolean state){
        btnAanpassenDocent.setEnabled(state);
        btnDeleteDocent.setEnabled(state);
    }
        
    private void btnNieuweDocentActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNieuweDocentActionPerformed
        // TODO add your handling code here:
        DocentNieuw scherm = new DocentNieuw();
        scherm.setDocentMain(this);
        scherm.setVisible(true);
    }//GEN-LAST:event_btnNieuweDocentActionPerformed

    private void btnAanpassenDocentActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAanpassenDocentActionPerformed
        // TODO add your handling code here:
        DocentNieuw scherm = new DocentNieuw();
        scherm.setDocentMain(this);
        model.PersoonModel persoonModel = (model.PersoonModel) docentList.getSelectedValue();
        scherm.vulScherm(persoonModel);
        scherm.setVisible(true);
    }//GEN-LAST:event_btnAanpassenDocentActionPerformed

    private void docentListValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_docentListValueChanged
        // TODO add your handling code here:
        if(docentList.isSelectionEmpty())
            this.enableButtons(false);
        else
            this.enableButtons(true);
    }//GEN-LAST:event_docentListValueChanged

    private void btnDeleteDocentActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteDocentActionPerformed
        // TODO add your handling code here:
        Object[] options = {
            "Verwijder",
            "Annuleren",};

        int iSelectedOption = JOptionPane.showOptionDialog(this,
                "Weet u zeker dat u deze speler uit het systeem wilt verwijderen?",
                "Bevestiging",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE,
                null,
                options,
                options[1]);

        if (iSelectedOption == JOptionPane.YES_OPTION) {
            model.PersoonModel persoonModel = (model.PersoonModel) docentList.getSelectedValue();
            persoonModel.deleteDocent();
            this.vulDocentLijst();
        }
    }//GEN-LAST:event_btnDeleteDocentActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /*
         * Set the Nimbus look and feel
         */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /*
         * If Nimbus (introduced in Java SE 6) is not available, stay with the
         * default look and feel. For details see
         * http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(DocentMain.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(DocentMain.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(DocentMain.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(DocentMain.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /*
         * Create and display the form
         */
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                new DocentMain().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAanpassenDocent;
    private javax.swing.JButton btnDeleteDocent;
    private javax.swing.JButton btnNieuweDocent;
    private javax.swing.JList docentList;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
}
