/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.*;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
/**
 *
 * @author Robin
 */
public class GeschiedenisModel {
    
    private int gsc_id;
    private int crs_id;
    private int dlr_id;
    private int spt_id;
    private String sport_type;
    private String deelnemer_naam;
    private String start_datum;
    
    public static ArrayList<GeschiedenisModel> findGeschiedenis()
    {
        
	ArrayList<GeschiedenisModel> allRecords = new ArrayList<GeschiedenisModel>();
        
        try{
            Connection conn = components.SimpleDataSource.getInstance();
            Statement stmt = conn.createStatement();
            ResultSet results = stmt.executeQuery("SELECT d.naam, s.sport_type, "
                    + "c.start_datum FROM geschiedenis g INNER JOIN deelnemer d "
                    + "ON (g.dlr_id=d.dlr_id) INNER JOIN sport s ON g.spt_id=s.spt_id "
                    + "INNER JOIN cursus c ON s.spt_id=c.spt_id");
            while(results.next()){
                GeschiedenisModel geschiedenis = new GeschiedenisModel();
                geschiedenis.setDeelnemerNaam(results.getString("naam"));
                geschiedenis.setSportType(results.getString("sport_type"));
                allRecords.add(geschiedenis);
            }
            
            results.close(); stmt.close();
            
        } 
        catch(SQLException e){
            System.out.println(e.getMessage());
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }
        return allRecords;
    }
    
    public int getDlrId() {
        return dlr_id;
    }
    
    public void setDlrId(int dlr_id) {
        this.dlr_id = dlr_id;
    }
    
    public int getCrsId() {
        return crs_id;
    }
    
    public void setCrsId(int crs_id) {
        this.crs_id = crs_id;
    }
    
    public void setDeelnemerNaam(String deelnemer_naam) {
        this.deelnemer_naam = deelnemer_naam;
    }
    
    public String getDeelnemerNaam() {
        return deelnemer_naam;
    }
    
    public void setSportType(String sport_type) {
        this.sport_type = sport_type;
    }
    
    public String getSportType() {
        return sport_type;
    }
    
    public String toString(){
        return this.getDeelnemerNaam() + " " +  this.getSportType();
    }
}
