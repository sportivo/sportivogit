/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.*;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Robin
 */
public class PersoonModel {
    private int dlr_id;
    private int doc_id;
    private int spt_id;
    private int crs_id;
    private String naam;
    private String adres;
    private String postcode;
    private String woonplaats;
    private String telefoonnummer;
    private String emailadres;
    private String bankrekening;
    
    public void saveDocent()
    {
        try{
            Connection conn = components.SimpleDataSource.getInstance();
            PreparedStatement stmt; 
            
            if(this.getDlr_Id() == 0){
                stmt = conn.prepareStatement("INSERT INTO docent "
                            + "(crs_id, spt_id, naam, adres, postcode, woonplaats, telefoonnummer, emailadres, bankrekening) "
                            + "VALUES"
                            + "(?, ?, ?, ?, ?, ?, ?, ?, ?)");
            }
            else{
                stmt = conn.prepareStatement("UPDATE deelnemer "
                            + "SET crs_id=?, spt_id=?, naam=?, adres=?, postcode=?, woonplaats=?, telefoonnummer=?, bankrekening=?"
                            + ", emailadres=? "
                            + "WHERE doc_id=?");
                stmt.setInt(10, this.getDoc_Id());
            }
            
            stmt.setInt(1, this.getCrs_Id());
            stmt.setInt(2, this.getSpt_Id());
            stmt.setString(3, this.getNaam());
            stmt.setString(4, this.getAdres());
            stmt.setString(5, this.getPostcode());
            stmt.setString(6, this.getWoonplaats());
            stmt.setString(7, this.getTelefoon());
            stmt.setString(8, this.getEmailAdres());
            stmt.setString(9, this.getBankrekening());
            stmt.execute(); stmt.close();
            
        }
        catch(SQLException e){
            System.out.println(e.getMessage());
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
    
    public void saveDeelnemer()
    {
        try{
            Connection conn = components.SimpleDataSource.getInstance();
            PreparedStatement stmt; 
            
            if(this.getDlr_Id() == 0){
                stmt = conn.prepareStatement("INSERT INTO deelnemer "
                            + "(naam, adres, postcode, woonplaats, telefoonnummer, emailadres) "
                            + "VALUES"
                            + "(?, ?, ?, ?, ?, ?)");
            }
            else{
                stmt = conn.prepareStatement("UPDATE deelnemer "
                            + "SET naam=?, adres=?, postcode=?, woonplaats=?, telefoonnummer=?"
                            + ", emailadres=? "
                            + "WHERE dlr_id=?");
                stmt.setInt(7, this.getDlr_Id());
            }
            
            stmt.setString(1, this.getNaam());
            stmt.setString(2, this.getAdres());
            stmt.setString(3, this.getPostcode());
            stmt.setString(4, this.getWoonplaats());
            stmt.setString(5, this.getTelefoon());
            stmt.setString(6, this.getEmailAdres());
            stmt.execute(); stmt.close();
            
        }
        catch(SQLException e){
            System.out.println(e.getMessage());
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
    
    public static ArrayList<PersoonModel> findAllDeelnemers()
    {
        ArrayList<PersoonModel> alDeelnemers = new ArrayList<PersoonModel>();
        try{
            Connection conn = components.SimpleDataSource.getInstance();
            Statement stmt = conn.createStatement();
            ResultSet results = stmt.executeQuery("SELECT * FROM deelnemer");
            
            while(results.next()){
                PersoonModel persoon = new PersoonModel();
                persoon.setDlr_Id(results.getInt("dlr_id"));
                persoon.setNaam(results.getString("naam"));
                persoon.setTelefoon(results.getString("telefoonnummer"));
                persoon.setEmailAdres(results.getString("emailadres"));
                persoon.setWoonplaats(results.getString("woonplaats"));
                persoon.setAdres(results.getString("adres"));
                persoon.setPostcode(results.getString("postcode"));
                alDeelnemers.add(persoon);
            }
            
            results.close(); stmt.close();
        } 
        catch(SQLException e){
            System.out.println(e.getMessage());
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }
        
        return alDeelnemers;
    }
    
    public static ArrayList<PersoonModel> findAllDocenten()
    {
        ArrayList<PersoonModel> alDocenten = new ArrayList<PersoonModel>();
        try{
            Connection conn = components.SimpleDataSource.getInstance();
            Statement stmt = conn.createStatement();
            ResultSet results = stmt.executeQuery("SELECT * FROM docent");
            
            while(results.next()){
                PersoonModel persoon = new PersoonModel();
                persoon.setDoc_Id(results.getInt("doc_id"));
                persoon.setSpt_Id(results.getInt("spt_id"));
                persoon.setCrs_Id(results.getInt("crs_id"));
                persoon.setNaam(results.getString("naam"));
                persoon.setAdres(results.getString("adres"));
                persoon.setPostcode(results.getString("postcode"));
                persoon.setWoonplaats(results.getString("woonplaats"));
                persoon.setTelefoon(results.getString("telefoonnummer"));
                persoon.setEmailAdres(results.getString("emailadres"));
                persoon.setBankrekening(results.getString("bankrekening"));
                alDocenten.add(persoon);
            }
            
            results.close(); stmt.close();
        } 
        catch(SQLException e){
            System.out.println(e.getMessage());
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }
        
        return alDocenten;
    }
    
    public void deleteDeelnemer()
    {
        try {
            Connection conn = components.SimpleDataSource.getInstance();
            PreparedStatement stmt = conn.prepareStatement("DELETE FROM deelnemer WHERE dlr_id=?");
            stmt.setInt(1, this.getDlr_Id());
            stmt.execute(); stmt.close();
            
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
    
    public void deleteDocent()
    {
        try {
            Connection conn = components.SimpleDataSource.getInstance();
            PreparedStatement stmt = conn.prepareStatement("DELETE FROM docent WHERE doc_id=?");
            stmt.setInt(1, this.getDoc_Id());
            stmt.execute(); stmt.close();
            
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
        
    public void setBankrekening(String bankrekening) {
        this.bankrekening = bankrekening;
    }

    public String getBankrekening() {
        return bankrekening;
    }
            
    public void setCrs_Id(int crs_id) {
        this.crs_id = crs_id;
    }

    public int getCrs_Id() {
        return crs_id;
    }
    
    public void setDoc_Id(int doc_id) {
        this.doc_id = doc_id;
    }

    public int getDoc_Id() {
        return doc_id;
    }
    
    public void setSpt_Id(int spt_id) {
        this.spt_id = spt_id;
    }

    public int getSpt_Id() {
        return spt_id;
    }
    
    public void setDlr_Id(int dlr_id) {
        this.dlr_id = dlr_id;
    }

    public int getDlr_Id() {
        return dlr_id;
    }
    
    public void setNaam(String naam) {
        this.naam = naam;
    }

    public String getNaam() {
        return naam;
    }
    
    public void setTelefoon(String telefoonnummer) {
        this.telefoonnummer = telefoonnummer;
    }

    public String getTelefoon() {
        return telefoonnummer;
    }
    
    public void setEmailAdres(String emailadres) {
        this.emailadres = emailadres;
    }

    public String getEmailAdres() {
        return emailadres;
    }
    
    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getPostcode() {
        return postcode;
    }
    
    public void setWoonplaats(String woonplaats) {
        this.woonplaats = woonplaats;
    }

    public String getWoonplaats() {
        return woonplaats;
    }
    
    public void setAdres(String adres) {
        this.adres = adres;
    }

    public String getAdres() {
        return adres;
    }
    
    public String toString(){
        return this.getNaam() + " " +  this.getEmailAdres();
    }
}
