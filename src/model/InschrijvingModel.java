/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author prasa90
 */
public class InschrijvingModel {
    private int ins_id;
    private int dlr_id;
    private int spt_id;
    private int crs_id;
    private int is_belangstellend;
    private int is_betaald;
    private String deelnemerNaam;
    
    public void saveGS()
    {
        try{
            Connection conn = components.SimpleDataSource.getInstance();
            PreparedStatement stmt; 
                stmt = conn.prepareStatement("INSERT INTO geschiedenis (dlr_id, spt_id) VALUES (?, ?)");
                                System.out.println(getSptId());
                                stmt.setInt(1, this.getDlrId());
				stmt.setInt(2, this.getSptId());
            
           
                System.out.println(stmt);
            stmt.execute(); stmt.close();
            
        }
        catch(SQLException e){
            System.out.println(e.getMessage());
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
    
    public void save()
    {
        try{
            Connection conn = components.SimpleDataSource.getInstance();
            PreparedStatement stmt = conn.prepareStatement("SELECT * FROM inschrijving WHERE dlr_id = ? AND crs_id = ?");
            System.out.println(getDlrId());
            stmt.setInt(1, this.getDlrId());
            stmt.setInt(2, this.getCrsId());
			ResultSet results = stmt.executeQuery();
			
			PreparedStatement stmt2 = null;
                        PreparedStatement stmt3 = null;
			if (!results.isBeforeFirst() ) {     //no data
				stmt2 = conn.prepareStatement("INSERT INTO inschrijving (dlr_id, crs_id, is_betaald, is_belangstellend) VALUES (?, ?, ?, ?)");
				stmt2.setInt(1, this.getDlrId());
				stmt2.setInt(2, this.getCrsId());
                                stmt2.setInt(3, this.getIsBetaald());
                                stmt2.setInt(4, this.getBelangstellend());
			}
			else{
				stmt2 = conn.prepareStatement("UPDATE inschrijving SET is_betaald=?, is_belangstellend=? WHERE dlr_id=? AND crs_id=?");
                                stmt2.setInt(1, this.getIsBetaald());
                                stmt2.setInt(2, this.getBelangstellend());
                                stmt2.setInt(3, this.getDlrId());
				stmt2.setInt(4, this.getCrsId());
			}
			
			results.close(); stmt.close(); //close first statement
			stmt2.execute(); stmt2.close(); //close second statement
            
        }
        catch(SQLException e){
            System.out.println("sqlfout: " + e.getMessage());
        }
        catch(Exception e){
            System.out.println("vage fout:" + e.getMessage());
        }
    }
    
    public int getSptId() {
        return spt_id;
    }
    
    public void setSptId(int spt_id) {
        this.spt_id = spt_id;
    }
    
    public int getInsId() {
        return ins_id;
    }
    
    public void setInsId(int ins_id) {
        this.ins_id = ins_id;
    }
    
    public int getDlrId() {
        return dlr_id;
    }
    
    public void setDlrId(int dlr_id) {
        this.dlr_id = dlr_id;
    }
    
    public int getCrsId() {
        return crs_id;
    }
    
    public void setCrsId(int crs_id) {
        this.crs_id = crs_id;
    }
    
    public int getBelangstellend() {
        return is_belangstellend;
    }
    
    public void setBelangstellend(int is_belangstellend) {
        this.is_belangstellend = is_belangstellend;
    }
    
    public int getIsBetaald() {
        return is_betaald;
    }
    
    public void setIsBetaald(int is_betaald) {
        this.is_betaald = is_betaald;
    }
    
    public String getDeelnemernaam() {
        return deelnemerNaam;
    }
    
    public void setDeelnemernaam(String deelnemerNaam) {
        this.deelnemerNaam = deelnemerNaam;
    }
}
