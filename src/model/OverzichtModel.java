/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.*;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Robin
 */
public class OverzichtModel {
    
    private String deelnemernaam;
    private String docentnaam;
    private String cursus_naam;
    private double kosten_sport;
    private String data;
        
public static ArrayList<OverzichtModel> findAllSportOverzicht()
    {
        
 ArrayList<OverzichtModel> allOverzichten = new ArrayList<OverzichtModel>();
        
        try{
            Connection conn = components.SimpleDataSource.getInstance();
            Statement stmt = conn.createStatement();
            ResultSet results = stmt.executeQuery("SELECT c.cursus_naam, s.kosten_sport FROM cursus c INNER JOIN sport s ON (s.spt_id = c.spt_id)");
            System.out.println(results);
            while(results.next()){
                OverzichtModel deelnemeroverzicht = new OverzichtModel();
                deelnemeroverzicht.setCursusNaam(results.getString("cursus_naam"));
                deelnemeroverzicht.setKostenSport(results.getDouble("kosten_sport"));
                deelnemeroverzicht.setData(results.getString("cursus_naam") + " " + results.getString("kosten_sport"));
                System.out.println();
                allOverzichten.add(deelnemeroverzicht);
            }
            
            results.close(); stmt.close();
            
        } 
        catch(SQLException e){
            System.out.println(e.getMessage());
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }
        return allOverzichten;
    }
    
public static ArrayList<OverzichtModel> findAllDeelnemerOverzicht()
    {
        
 ArrayList<OverzichtModel> allOverzichten = new ArrayList<OverzichtModel>();
        
        try{
            Connection conn = components.SimpleDataSource.getInstance();
            Statement stmt = conn.createStatement();
            ResultSet results = stmt.executeQuery("SELECT d.naam FROM deelnemer d JOIN geschiedenis g ON g.dlr_id = d.dlr_id JOIN sport s ON s.spt_id = g.spt_id WHERE s.sport_type NOT LIKE '%tafeltennis%'");
            System.out.println(results);
            while(results.next()){
                OverzichtModel deelnemeroverzicht = new OverzichtModel();
                deelnemeroverzicht.setDeelnemerNaam(results.getString("naam"));
                deelnemeroverzicht.setData(results.getString("naam"));
                allOverzichten.add(deelnemeroverzicht);
            }
            
            results.close(); stmt.close();
            
        } 
        catch(SQLException e){
            System.out.println(e.getMessage());
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }
        return allOverzichten;
    }


public static ArrayList<OverzichtModel> findAllDocentOverzicht()
    {
        
 ArrayList<OverzichtModel> allOverzichten = new ArrayList<OverzichtModel>();
        
        try{
            Connection conn = components.SimpleDataSource.getInstance();
            Statement stmt = conn.createStatement();
            ResultSet results = stmt.executeQuery("SELECT d.naam, c.cursus_naam FROM docent d INNER JOIN cursus c ON d.crs_id = c.crs_id");
            System.out.println(results);
            while(results.next()){
                OverzichtModel docentoverzicht = new OverzichtModel();
                docentoverzicht.setDocentNaam(results.getString("naam"));
                docentoverzicht.setCursusNaam(results.getString("cursus_naam"));
                docentoverzicht.setData(results.getString("naam") + " " + results.getString("cursus_naam"));
                allOverzichten.add(docentoverzicht);
            }
            
            results.close(); stmt.close();
            
        } 
        catch(SQLException e){
            System.out.println(e.getMessage());
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }
        return allOverzichten;
    }
    
    
    
    public void setDocentNaam(String docentnaam) {
        this.docentnaam = docentnaam;
    }
    
    public String getDocentNaam() {
        return docentnaam;
    }
    
    public void setDeelnemerNaam(String deelnemernaam) {
        this.deelnemernaam = deelnemernaam;
    }
    
    public String getDeelnemerNaam() {
        return deelnemernaam;
    }
    
    public void setCursusNaam(String cursus_naam) {
        this.cursus_naam = cursus_naam;
    }
    
    public String getCursusNaam() {
        return cursus_naam;
    }
    
    public void setData(String data) {
        this.data = data;
    }
    
    public String getData() {
        return data;
    }
    
    public double getKostenSport() {
        return kosten_sport;
    }

    public void setKostenSport(double kosten_sport) {
        this.kosten_sport = kosten_sport;
    }
    
@Override
        public String toString(){
            return this.getData();
        }

}
