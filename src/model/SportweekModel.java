/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.*;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
/**
 *
 * @author Robin
 */
public class SportweekModel {
    private int spw_id;
    private int crs_id;
    private String start_datum;
    private String eind_datum;
    private String cursus_naam;
    private String naam;
    
    public static ArrayList<SportweekModel> findAllSportWeken()
    {
        
	ArrayList<SportweekModel> allSportenWeken = new ArrayList<SportweekModel>();
        
        try{
            Connection conn = components.SimpleDataSource.getInstance();
            Statement stmt = conn.createStatement();
            ResultSet results = stmt.executeQuery("SELECT * FROM Sport");
            System.out.println(results);
            while(results.next()){
                SportweekModel sportWeek = new SportweekModel();
                sportWeek.setSpwId(results.getInt("spw_id"));
                sportWeek.setCursusId(results.getInt("crs_id"));
                sportWeek.setNaam(results.getString("naam"));
                sportWeek.setStartDatum(results.getString("start_datum"));
                sportWeek.setEindDatum(results.getString("eind_datum"));
                allSportenWeken.add(sportWeek);
            }
            
            results.close(); stmt.close();
            
        } 
        catch(SQLException e){
            System.out.println(e.getMessage());
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }
        return allSportenWeken;
    }
    
    public void save()
    {
        try{
            Connection conn = components.SimpleDataSource.getInstance();
            PreparedStatement stmt; 
            
            if(this.getCursusId() == 0){
                stmt = conn.prepareStatement("INSERT INTO sportweek "
                            + "(crs_id, naam, start_datum, eind_datum) "
                            + "VALUES"
                            + "(?, ?, ?, ?)");
            }
            else{
                
                stmt = conn.prepareStatement("UPDATE sportweek "
                            + "SET crs_id=?, naam=?, start_datum=?, eind_datum=? "
                            + "WHERE crs_id=?");
                stmt.setInt(4, this.getSpwId());
            }
            
            stmt.setInt(1, this.getCursusId());     
            stmt.setString(2, this.getNaam());
            stmt.setString(3, this.getStartDatum());
            stmt.setString(4, this.getEindDatum());
                System.out.println(stmt);
            stmt.execute(); stmt.close();
            
        }
        catch(SQLException e){
            System.out.println(e.getMessage());
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
    
    public void nieuweCursus()
    {
        try{
            Connection conn = components.SimpleDataSource.getInstance();
            PreparedStatement stmt; 
            
            
                stmt = conn.prepareStatement("INSERT INTO sportweek "
                            + "(crs_id, naam, start_datum, eind_datum) "
                            + "VALUES"
                            + "(?, ?, ?, ?)");
            
            stmt.setInt(1, this.getCursusId());     
            stmt.setString(2, this.getNaam());
            stmt.setString(3, this.getStartDatum());
            stmt.setString(4, this.getEindDatum());
                System.out.println(stmt);
            stmt.execute(); stmt.close();
            
        }
        catch(SQLException e){
            System.out.println(e.getMessage());
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
    
    public int getSpwId() {
        return spw_id;
    }
    
    public void setSpwId(int spw_id) {
        this.spw_id = spw_id;
    }
    
    public int getCursusId() {
        return crs_id;
    }
    
    public void setCursusId(int crs_id) {
        this.crs_id = crs_id;
    }
    
    public String getStartDatum() {
        return start_datum;
    }
    
    public void setStartDatum(String start_datum) {
        this.start_datum = start_datum;
    }
    
    public String getEindDatum() {
        return eind_datum;
    }
    
    public void setEindDatum(String eind_datum) {
        this.eind_datum = eind_datum;
    }
    
    public String getCursusNaam() {
        return cursus_naam;
    }
    
    public void setCursusNaam(String cursus_naam) {
        this.cursus_naam = cursus_naam;
    }
    
    public String getNaam() {
        return naam;
    }
    
    public void setNaam(String naam) {
        this.naam = naam;
    }
    
    public String toString(){
        return this.getNaam() + " start " +  this.getStartDatum() + " eind " + this.getEindDatum();
    }

}
