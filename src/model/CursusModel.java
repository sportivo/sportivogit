/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.*;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Robin
 */
public class CursusModel {
    private int cursusId;
    private String startDatum;
    private String cursusNaam;
    private int cursusSport;
    private String sport_naam;
    
    public void delete()
    {
        try {
            Connection conn = components.SimpleDataSource.getInstance();
            PreparedStatement stmt = conn.prepareStatement("DELETE FROM Cursus WHERE crs_id=?");
            stmt.setInt(1, this.getCursusId());
            stmt.execute(); stmt.close();
            
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
    
    public static ArrayList<CursusModel> findAllCursussen()
    {
        
	ArrayList<CursusModel> allCursussen = new ArrayList<CursusModel>();
        
        try{
            Connection conn = components.SimpleDataSource.getInstance();
            Statement stmt = conn.createStatement();
            ResultSet results = stmt.executeQuery("SELECT c.crs_id, c.spt_id, c.start_datum, c.cursus_naam, s.sport_type FROM Cursus c INNER JOIN sport s ON (s.spt_id=c.spt_id)");
            System.out.println(results);
            while(results.next()){
                CursusModel cursus = new CursusModel();
                cursus.setCursusId(results.getInt("crs_id"));
                cursus.setCursusSport(results.getInt("spt_id"));
                cursus.setStartDatum(results.getString("start_datum"));
                cursus.setCursusNaam(results.getString("cursus_naam"));
                cursus.setSportNaam(results.getString("sport_type"));
                allCursussen.add(cursus);
            }
            
            results.close(); stmt.close();
            
        } 
        catch(SQLException e){
            System.out.println(e.getMessage());
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }
        return allCursussen;
    }
    
    public void save()
    {
        try{
            Connection conn = components.SimpleDataSource.getInstance();
            PreparedStatement stmt; 
            
            if(this.getCursusId() == 0){
                stmt = conn.prepareStatement("INSERT INTO cursus "
                            + "(spt_id, cursus_naam, start_datum) "
                            + "VALUES"
                            + "(?, ?, ?)");
            }
            else{
                
                stmt = conn.prepareStatement("UPDATE cursus "
                            + "SET spt_id=?, cursus_naam=?, start_datum=? "
                            + "WHERE crs_id=?");
                stmt.setInt(4, this.getCursusId());
            }
            
            stmt.setInt(1, this.getCursusSport());     
            stmt.setString(2, this.getCursusNaam());
            stmt.setString(3, this.getStartDatum());
                System.out.println(stmt);
            stmt.execute(); stmt.close();
            
        }
        catch(SQLException e){
            System.out.println(e.getMessage());
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
    
    public String getSportNaam() {
        return sport_naam;
    }
    
    public void setSportNaam(String sport_naam) {
        this.sport_naam = sport_naam;
    }
    
    public int getCursusId() {
        return cursusId;
    }
    
    public void setCursusId(int cursusId) {
        this.cursusId = cursusId;
    }
    
    public String getStartDatum() {
        return startDatum;
    }
    
    public void setStartDatum(String startDatum) {
        this.startDatum = startDatum;
    }

    public void setCursusNaam(String cursusNaam) {
        this.cursusNaam = cursusNaam;
    }
    
    public String getCursusNaam() {
        return cursusNaam;
    }
    
    public void setCursusSport(int cursusSport) {
        this.cursusSport = cursusSport;
    }
    
    public int getCursusSport() {
        return cursusSport;
    }
    
    public String toString(){
        return this.getCursusNaam() + " " +  this.getSportNaam();
    }
}
