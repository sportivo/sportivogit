/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.sql.*;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
/**
 *
 * @author Robin
 */
public class SportModel {
    private int spt_id;
    private String sport_type;
    private double kosten_sport;
    private int maxdeelnemers_sport;
    
    public void delete()
    {
        try {
            Connection conn = components.SimpleDataSource.getInstance();
            PreparedStatement stmt = conn.prepareStatement("DELETE FROM sport WHERE spt_id=?");
            stmt.setInt(1, this.getSptId());
            stmt.execute(); stmt.close();
            
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
    
    public static ArrayList<SportModel> findAllSporten()
    {
        
	ArrayList<SportModel> allSporten = new ArrayList<SportModel>();
        
        try{
            Connection conn = components.SimpleDataSource.getInstance();
            Statement stmt = conn.createStatement();
            ResultSet results = stmt.executeQuery("SELECT * FROM Sport");
            System.out.println(results);
            while(results.next()){
                SportModel sport = new SportModel();
                sport.setSptId(results.getInt("spt_id"));
                sport.setSportType(results.getString("sport_type"));
                sport.setKostenSport(results.getDouble("kosten_sport"));
                sport.setMaxdeelnemers(results.getInt("maxdeelnemers_sport"));
                allSporten.add(sport);
            }
            
            results.close(); stmt.close();
            
        } 
        catch(SQLException e){
            System.out.println(e.getMessage());
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }
        return allSporten;
    }
    
    public void save()
    {
        try{
            Connection conn = components.SimpleDataSource.getInstance();
            PreparedStatement stmt; 
            
            if(this.getSptId() == 0){
                stmt = conn.prepareStatement("INSERT INTO sport "
                            + "(sport_type, kosten_sport, maxdeelnemers_sport) "
                            + "VALUES"
                            + "(?, ?, ?)");
            }
            else{
                
                stmt = conn.prepareStatement("UPDATE sport "
                            + "SET sport_type=?, kosten_sport=?, maxdeelnemers_sport=? "
                            + "WHERE spt_id=?");
                stmt.setInt(4, this.getSptId());
            }
            
            stmt.setString(1, this.getSportType());     
            stmt.setDouble(2, this.getKostenSport());
            stmt.setInt(3, this.getMaxdeelnemers());
            
            stmt.execute(); stmt.close();
            
        }
        catch(SQLException e){
            System.out.println(e.getMessage());
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
    
    public int getSptId() {
        return spt_id;
    }

    public void setSptId(int spt_id) {
        this.spt_id = spt_id;
    }
    
    public String getSportType() {
        return sport_type;
    }

    public void setSportType(String sport_type) {
        this.sport_type = sport_type;
    }
    
    public double getKostenSport() {
        return kosten_sport;
    }

    public void setKostenSport(double kosten_sport) {
        this.kosten_sport = kosten_sport;
    }
    
    public int getMaxdeelnemers() {
        return maxdeelnemers_sport;
    }

    public void setMaxdeelnemers(int maxdeelnemers_sport) {
        this.maxdeelnemers_sport = maxdeelnemers_sport;
    }
    
    public String toString(){
        return this.getSportType();
    }
}
